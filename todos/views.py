from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import CreateTodoForm


# Create your views here.


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "name": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": details,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = CreateTodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = CreateTodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    ex_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = CreateTodoForm(request.POST, instance=ex_list)
        if form.is_valid():
            ex_list = form.save()
            return redirect("todo_list_detail", id=ex_list.id)
    else:
        form = CreateTodoForm(instance=ex_list)
    context = {"form": form}
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    uw_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        uw_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
